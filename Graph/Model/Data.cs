﻿//-----------------------------------------------------------------------
// <copyright file="Data.cs" company="RBC 2016">
//     Copyright (c) RBC 2016. All rights reserved.
// </copyright>
// <summary>This class provides data to the class.</summary>
// <author>Robert Carswell</author>
//-----------------------------------------------------------------------

namespace Graph.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Data class provides sample data to graph form.
    /// </summary>
    public static class Data
    {
        /// <summary>
        /// Method for [y = 2x] graph.
        /// </summary>
        /// <param name="xValue">The x value.</param>
        /// <returns>The y value.</returns>
        public static int GetData1(int xValue)
        {
            int yValue = 2 * xValue;
            return yValue;
        }

        /// <summary>
        /// Method for [y = x^2 - 1] graph.
        /// </summary>
        /// <param name="xValue">The x value.</param>
        /// <returns>The y value.</returns>
        public static int GetData2(int xValue)
        {
            int yValue = (int)Math.Pow(xValue, 2) - 1;
            return yValue;
        }
    }
}
