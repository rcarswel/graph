﻿namespace Graph
{
    partial class GraphSample
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.rbLine = new System.Windows.Forms.RadioButton();
            this.rbColumn = new System.Windows.Forms.RadioButton();
            this.cbData1 = new System.Windows.Forms.CheckBox();
            this.cbData2 = new System.Windows.Forms.CheckBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nudXRange = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXRange)).BeginInit();
            this.SuspendLayout();
            // 
            // chart
            // 
            chartArea2.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart.Legends.Add(legend2);
            this.chart.Location = new System.Drawing.Point(13, 13);
            this.chart.Name = "chart";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chart.Series.Add(series2);
            this.chart.Size = new System.Drawing.Size(559, 415);
            this.chart.TabIndex = 0;
            this.chart.Text = "Graph";
            // 
            // rbLine
            // 
            this.rbLine.AutoSize = true;
            this.rbLine.Checked = true;
            this.rbLine.Location = new System.Drawing.Point(84, 440);
            this.rbLine.Name = "rbLine";
            this.rbLine.Size = new System.Drawing.Size(45, 17);
            this.rbLine.TabIndex = 1;
            this.rbLine.TabStop = true;
            this.rbLine.Text = "Line";
            this.rbLine.UseVisualStyleBackColor = true;
            this.rbLine.CheckedChanged += new System.EventHandler(this.GraphSample_Load);
            // 
            // rbColumn
            // 
            this.rbColumn.AutoSize = true;
            this.rbColumn.Location = new System.Drawing.Point(135, 440);
            this.rbColumn.Name = "rbColumn";
            this.rbColumn.Size = new System.Drawing.Size(60, 17);
            this.rbColumn.TabIndex = 2;
            this.rbColumn.Text = "Column";
            this.rbColumn.UseVisualStyleBackColor = true;
            this.rbColumn.CheckedChanged += new System.EventHandler(this.GraphSample_Load);
            // 
            // cbData1
            // 
            this.cbData1.AutoSize = true;
            this.cbData1.Location = new System.Drawing.Point(240, 441);
            this.cbData1.Name = "cbData1";
            this.cbData1.Size = new System.Drawing.Size(37, 17);
            this.cbData1.TabIndex = 3;
            this.cbData1.Text = "2x";
            this.cbData1.UseVisualStyleBackColor = true;
            this.cbData1.CheckStateChanged += new System.EventHandler(this.GraphSample_Load);
            // 
            // cbData2
            // 
            this.cbData2.AutoSize = true;
            this.cbData2.Location = new System.Drawing.Point(283, 441);
            this.cbData2.Name = "cbData2";
            this.cbData2.Size = new System.Drawing.Size(58, 17);
            this.cbData2.TabIndex = 4;
            this.cbData2.Text = "x^2 - 1";
            this.cbData2.UseVisualStyleBackColor = true;
            this.cbData2.CheckedChanged += new System.EventHandler(this.GraphSample_Load);
            this.cbData2.CheckStateChanged += new System.EventHandler(this.GraphSample_Load);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(449, 434);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(123, 23);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.BTNPrint_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(201, 440);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Data:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 440);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Graph Type:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(348, 440);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "X-Range";
            // 
            // nudXRange
            // 
            this.nudXRange.Location = new System.Drawing.Point(404, 437);
            this.nudXRange.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudXRange.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudXRange.Name = "nudXRange";
            this.nudXRange.ReadOnly = true;
            this.nudXRange.Size = new System.Drawing.Size(39, 20);
            this.nudXRange.TabIndex = 9;
            this.nudXRange.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudXRange.ValueChanged += new System.EventHandler(this.GraphSample_Load);
            // 
            // GraphSample
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 462);
            this.Controls.Add(this.nudXRange);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.cbData2);
            this.Controls.Add(this.cbData1);
            this.Controls.Add(this.rbColumn);
            this.Controls.Add(this.rbLine);
            this.Controls.Add(this.chart);
            this.Name = "GraphSample";
            this.Text = "Sample Graph";
            this.Load += new System.EventHandler(this.GraphSample_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXRange)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart;
        private System.Windows.Forms.RadioButton rbLine;
        private System.Windows.Forms.RadioButton rbColumn;
        private System.Windows.Forms.CheckBox cbData1;
        private System.Windows.Forms.CheckBox cbData2;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudXRange;
    }
}

