﻿//-----------------------------------------------------------------------
// <copyright file="GraphSample.cs" company="RBC 2016">
//     Copyright (c) RBC 2016. All rights reserved.
// </copyright>
// <summary>This is the form view for a Graph Sample.</summary>
// <author>Robert Carswell</author>
//-----------------------------------------------------------------------

namespace Graph
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Drawing.Printing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.Windows.Forms.DataVisualization.Charting;
    using Graph.Model;

    /// <summary>
    /// Form that displays a complete sample graph.
    /// </summary>
    public partial class GraphSample : Form
    {
        /// <summary>The maximum chart.</summary>
        private int maxValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphSample"/> class.
        /// </summary>
        public GraphSample()
        {
            this.maxValue = 220;
            this.InitialLook();
            this.InitializeComponent();
        }

        /// <summary>
        /// Loads the graphs information.
        /// </summary>
        /// <param name="sender">Sending object.</param>
        /// <param name="e">Click on object.</param>
        public void GraphSample_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
            this.RunReport();
            System.Windows.Forms.Cursor.Current = Cursors.Default;
        }

        /// <summary>
        /// Runs the report chart data.
        /// </summary>
        private void RunReport()
        {
            if (this.cbData1.Checked == false && this.cbData2.Checked == false)
            {
                this.cbData1.Checked = true;
            }

            // Clear Chart
            this.chart.Series.Clear();
            this.chart.Legends.Clear();
            this.chart.ChartAreas.Clear();
            this.chart.Titles.Clear();

            // Build Chart
            SeriesChartType chartType = this.ChartType();
            this.maxValue = 0;
            string title = "Data";

            if (this.cbData1.Checked == true)
            {
                int data = 1;
                string name = "y = 2x";
                this.ChartSeries(data, name, System.Drawing.Color.Blue, chartType);
            }

            if (this.cbData2.Checked == true)
            {
                int data = 2;
                string name = "y = x^2 -1";
                this.ChartSeries(data, name, System.Drawing.Color.Green, chartType);
            }

            this.ChartAreas(this.maxValue, title);
            this.ChartTitle(title);

            this.chart.Invalidate();
        }

        /// <summary>
        /// Sets up the look and style of the chart, Areas.
        /// </summary>
        /// <param name="max">Maximum chart value.</param>
        /// <param name="title">Title of the chart.</param>
        private void ChartAreas(double max, string title)
        {
            if (max == 0)
            {
                max = 220;
            }

            var axisX = new System.Windows.Forms.DataVisualization.Charting.Axis
            {
                Interval = 1,
            };

            var axisY = new System.Windows.Forms.DataVisualization.Charting.Axis
            {
                Minimum = 0,
                Maximum = max,
                Title = title,
            };

            var chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea
            {
                AxisX = axisX,
                AxisY = axisY,
            };

            this.chart.ChartAreas.Add(chartArea1);
        }

        /// <summary>
        /// Sets up the look and style of the chart, Title.
        /// </summary>
        /// <param name="title">Title of the chart.</param>
        private void ChartTitle(string title)
        {
            var titles1 = new System.Windows.Forms.DataVisualization.Charting.Title
            {
                Name = title,
                Text = "Sample Graph's " + title,
                Visible = true,
            };
            this.chart.Titles.Add(titles1);
        }

        /// <summary>
        /// Sets up the look and style of the chart, Legends.
        /// </summary>
        /// <param name="name">Name of the chart data.</param>
        private void ChartLegends(string name)
        {
            var legends1 = new System.Windows.Forms.DataVisualization.Charting.Legend
            {
                Name = name,
            };
            this.chart.Legends.Add(legends1);
        }

        /// <summary>
        /// Sets up the look and style of the chart, Series.
        /// </summary>
        /// <param name="data">The data type.</param>
        /// <param name="name">The name of the data.</param>
        /// <param name="color">The color of the line.</param>
        /// <param name="chartType">Type of chart to display.</param>
        private void ChartSeries(int data, string name, Color color, SeriesChartType chartType)
        {
            var series1 = new System.Windows.Forms.DataVisualization.Charting.Series
            {
                Name = name,
                Color = color,
                BorderWidth = 5,
                IsVisibleInLegend = true,
                IsXValueIndexed = true,
                ChartType = chartType,
                MarkerStyle = MarkerStyle.Circle,
            };

            int xValues = (int)this.nudXRange.Value;

            for (int i = 0; i < (xValues + 1); i++)
            {
                int yValue = 0;
                if (data == 1)
                {
                    yValue = Data.GetData1(i);
                }
                else if (data == 2)
                {
                    yValue = Data.GetData2(i);
                }

                series1.Points.AddXY(i, yValue);
                if ((yValue * 1.1) > this.maxValue)
                {
                    this.maxValue = Convert.ToInt32(yValue * 1.1);
                }
            }

            this.chart.Series.Add(series1);
            this.ChartLegends(name);
        }

        /// <summary>
        /// Returns the chart type.
        /// </summary>
        /// <returns>The chart type.</returns>
        private SeriesChartType ChartType()
        {
            if (this.rbLine.Checked)
            {
                return SeriesChartType.Line;
            }
            else if (this.rbColumn.Checked)
            {
                return SeriesChartType.Column;
            }
            else
            {
                return SeriesChartType.Line;
            }
        }

        /// <summary>
        /// Creates the initial look.
        /// </summary>
        private void InitialLook()
        {
            // Radio Buttons
            this.rbLine = new RadioButton();
            this.rbLine.Checked = true;
            this.rbColumn = new RadioButton();
            this.rbColumn.Checked = false;
        }

        /// <summary>
        /// Button that prints current chart data.
        /// </summary>
        /// <param name="sender">Sending object.</param>
        /// <param name="e">Click on object.</param>
        private void BTNPrint_Click(object sender, EventArgs e)
        {
            var pd = new System.Drawing.Printing.PrintDocument();
            pd.PrintPage += new PrintPageEventHandler(this.PrintChart);
            pd.DefaultPageSettings.Landscape = true;

            PrintPreviewDialog pdi = new PrintPreviewDialog();
            pdi.Document = pd;
            if (pdi.ShowDialog() == DialogResult.OK)
            {
                pdi.Document.Print();
            }
        }

        /// <summary>
        /// Sets up the chart to print.
        /// </summary>
        /// <param name="sender">Sending object.</param>
        /// <param name="ev">Click on object.</param>
        private void PrintChart(object sender, PrintPageEventArgs ev)
        {
            using (var f = new System.Drawing.Font("Arial", 10))
            {
                var size = ev.Graphics.MeasureString(Text, f);
                ev.Graphics.DrawString(string.Empty, f, Brushes.Black, ev.PageBounds.X + ((ev.PageBounds.Width - size.Width) / 2), ev.PageBounds.Y);
            }

            // Note, the chart printing code wants to print in pixels.
            Rectangle marginBounds = ev.MarginBounds;
            if (ev.Graphics.PageUnit != GraphicsUnit.Pixel)
            {
                ev.Graphics.PageUnit = GraphicsUnit.Pixel;
                marginBounds.X = (int)(marginBounds.X * (ev.Graphics.DpiX / 100f));
                marginBounds.Y = (int)(marginBounds.Y * (ev.Graphics.DpiY / 100f));
                marginBounds.Width = (int)(marginBounds.Width * (ev.Graphics.DpiX / 100f));
                marginBounds.Height = (int)(marginBounds.Height * (ev.Graphics.DpiY / 100f));
            }

            this.chart.Printing.PrintPaint(ev.Graphics, marginBounds);
        }
    }
}