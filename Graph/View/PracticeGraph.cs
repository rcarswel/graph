﻿//-----------------------------------------------------------------------
// <copyright file="PracticeGraph.cs" company="RBC 2016">
//     Copyright (c) RBC 2016. All rights reserved.
// </copyright>
// <summary>This is the form view for you to write a practice graph.</summary>
// <author>Robert Carswell</author>
//-----------------------------------------------------------------------

namespace Graph.View
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.Windows.Forms.DataVisualization.Charting;
    using Graph.Model;

    /// <summary>
    /// The form for your to practice the graph.
    /// </summary>
    public partial class PracticeGraph : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PracticeGraph"/> class.
        /// </summary>
        public PracticeGraph()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Loads the graphs information.
        /// </summary>
        /// <param name="sender">Sending object.</param>
        /// <param name="e">Click on object.</param>
        private void PracticeGraph_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
            this.RunReport();
            System.Windows.Forms.Cursor.Current = Cursors.Default;
        }

        /// <summary>
        /// Runs the report chart data.
        /// </summary>
        private void RunReport()
        {
        }

        /// <summary>
        /// Sets up the look and style of the chart, Areas.
        /// </summary>
        /// <param name="title">Title of the chart.</param>
        private void ChartAreas(string title)
        { 
        }

        /// <summary>
        /// Sets up the look and style of the chart, Title.
        /// </summary>
        /// <param name="title">Title of the chart.</param>
        private void ChartTitle(string title)
        { 
        }

        /// <summary>
        /// Sets up the look and style of the chart, Legends.
        /// </summary>
        /// <param name="name">Name of the chart data.</param>
        private void ChartLegends(string name)
        { 
        }

        /// <summary>
        /// Sets up the look and style of the chart, Series.
        /// </summary>
        /// <param name="data">The data type.</param>
        /// <param name="name">The name of the data.</param>
        private void ChartSeries(int data, string name)
        { 
        }
    }
}
