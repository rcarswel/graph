﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="RBC 2016">
//     Copyright (c) RBC 2016. All rights reserved.
// </copyright>
// <summary>This program for the graph example.</summary>
// <author>Robert Carswell</author>
//-----------------------------------------------------------------------

namespace Graph
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Graph.View;

    /// <summary>
    /// The program for the graph example.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ////Application.Run(new GraphSample());
            ////Application.Run(new PracticeGraph());
            Application.Run(new VideoGraph());
        }
    }
}
